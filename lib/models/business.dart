import 'package:flutter/material.dart';

class Business {
  final bool active;
  final String userId;
  final String id;
  final String countryId; //
  final int type; // 0 = shelter | 1 = vet | 2 = store
  final String name; //
  final String pictureUrl; //
  final String adoptionLink; //
  final String businessColor; //

  final List<Map<String, String>> addresses; //
  final List<String> phone; //
  final List<Map<String, String>> openingHours; //
  final List<String> services; //
  final List<Map<String, String>> contact; //
  final Map<String, List<Map<String, String>>> donationAccounts;

  Business({
    this.active,
    this.userId,
    this.id,
    @required this.countryId,
    @required this.type,
    @required this.name,
    @required this.pictureUrl,
    this.adoptionLink,
    this.businessColor,

    this.addresses,
    this.phone,
    this.openingHours,
    this.services,
    this.contact,
    this.donationAccounts,
  });

  Map<String, dynamic> toMapNewBusiness() {
    return {
      'active': active,
      'userId': userId,
      'countryId': countryId,
      'type': type,
      'name': name,
      'pictureUrl': pictureUrl
    };
  }

  Map<String, dynamic> toMap() {
    return {
      'countryId': countryId,
      'type': type,
      'name': name,
      'pictureUrl': pictureUrl,
      'adoptionLink': adoptionLink,
      'businessColor': businessColor,

      'addresses': addresses == null ? [] : addresses.cast(),
      'phone': phone == null ? [] : phone.cast(),
      'openingHours': openingHours == null ? [] : openingHours.cast(),
      'services': services == null ? [] : services.cast(),
      'contact': contact == null ? [] : contact.cast(),
      'donationAccounts': donationAccounts == null ? {} : donationAccounts.cast()
    };
  }

  static Business buildBusiness(data) {
    return Business(
      id: data.documentID,
      type: data['type'],
      pictureUrl: data['pictureUrl'],
      name: data['name'],
      addresses: data['addresses'] != null ? data['addresses'].cast<Map<String, String>>() : [],
      phone: data['phone'] != null ? data['phone'].cast<String>() : [],
      openingHours: data['openingHours'] != null ? data['openingHours'].cast<Map<String, String>>() : [],
      services: data['services'] != null ? data['services'].cast<String>() : [],
      donationAccounts: data['donationAccounts'] != null ? data['donationAccounts'].cast<String, List<Map<String, String>>>() : {},
      contact: data['contact'] != null ? data['contact'].cast<Map<String, String>>() : [],
      adoptionLink: data['adoptionLink'],
      businessColor: data['businessColor'],
      countryId: data['countryId']
    );
  }
}
