import 'package:flutter/foundation.dart';

class Pet {
  final String id;
  final String businessId;
  final String name;
  final bool sex;
  final String age;
  final String pictureUrl;
  final String description;

  Pet({
    this.id,
    @required this.businessId,
    @required this.name,
    @required this.sex,
    @required this.age,
    @required this.pictureUrl,
    @required this.description
  });

  Map<String, dynamic> toMap() {
    return {
      'businessId': businessId,
      'name': name,
      'sex': sex,
      'age': age,
      'pictureUrl': pictureUrl,
      'description': description
    };
  }

  static Pet buildPet(data) {
    return Pet(
      id: data.documentID,
      businessId: data['businessId'],
      name: data['name'],
      sex: data['sex'],
      age: data['age'],
      pictureUrl: data['pictureUrl'],
      description: data['description']
    );
  }

}
