import 'package:flutter/foundation.dart';

class Product {
  final String id;
  final String businessId;
  final String name;
  final String currency;
  final double price;
  final String smallDescription;
  final String fullDescription;
  final String pictureUrl;

  Product({
    this.id,
    @required this.businessId,
    @required this.name,
    @required this.currency,
    @required this.price,
    @required this.smallDescription,
    @required this.fullDescription,
    this.pictureUrl
  });

  Map<String, dynamic> toMap() {
    return {
      'businessId': businessId,
      'name': name,
      'currency': currency,
      'price': price,
      'smallDescription': smallDescription,
      'fullDescription': fullDescription,
      'pictureUrl': pictureUrl
    };
  }

  static Product buildProduct(data) {
    return Product(
      id: data.documentID,
      businessId: data['businessId'],
      name: data['name'],
      currency: data['currency'],
      price: data['price'].toDouble(),
      smallDescription: data['smallDescription'],
      fullDescription: data['fullDescription'],
      pictureUrl: data['pictureUrl']
    );
  }

}
