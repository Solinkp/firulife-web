import 'package:flutter/material.dart';

import '../services/authentication.dart';
import '../widgets/spinner_loader.dart';
import './admin/admin_home_screen.dart';
import './business/business_home_screen.dart';
import './login_screen.dart';
import './not_allowed_screen.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN
}

class RootScreen extends StatefulWidget {
  final BaseAuth auth;

  RootScreen({this.auth});

  @override
  State<StatefulWidget> createState() => _RootScreenState();
}

class _RootScreenState extends State<RootScreen> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";
  int _type;
  String _displayName = "";

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user['user'].uid;
          _type = user['type'];
          _displayName = user['user'].displayName;
        }
        authStatus = user == null || user['user'].uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
  }

  void loginCallback() {
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user['user'].uid.toString();
          _type = user['type'];
          _displayName = user['user'].displayName;
        }
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void logoutCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
      _type = null;
      _displayName = "";
    });
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: SpinnerLoader()
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
      break;
      case AuthStatus.NOT_LOGGED_IN:
        return LoginScreen(
          auth: widget.auth,
          loginCallback: loginCallback
        );
      break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          switch(_type) {
            case 0:
              return AdminHomeScreen(
                userId: _userId,
                auth: widget.auth,
                logoutCallback: logoutCallback,
                displayName: _displayName
              );
            break;
            case 1:
              return BusinessHomeScreen(
                userId: _userId,
                auth: widget.auth,
                logoutCallback: logoutCallback,
                displayName: _displayName
              );
            break;
            default:
              return NotAllowedScreen(
                auth: widget.auth,
                logoutCallback: logoutCallback,
              );
          }
        } else
          return buildWaitingScreen();
      break;
      default:
        return buildWaitingScreen();
    }
  }
}
