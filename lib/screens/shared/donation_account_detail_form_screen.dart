import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/donation_account_detail_arguments.dart';
import '../../widgets/donation_account_detail_form.dart';

class DonationAccountDetailFormScreen extends StatelessWidget {
  static String routeName = '/donation_account_detail_form';

  @override
  Widget build(BuildContext context) {
    final DonationAccountDetailArguments _args = ModalRoute.of(context).settings.arguments;
    final Business _business = _args.business;
    final String _mapKey = _args.mapKey;
    final int _index = _args.index;
    final String _title = _mapKey == null ? 'Nueva Cuenta' : 'Editar Cuenta';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$_title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: DonationAccountDetailForm(_business, _mapKey, _index)
    );
  }
}
