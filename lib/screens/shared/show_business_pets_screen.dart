import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/business.dart';
import '../../models/pet.dart';
import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';
import '../../settings/pet_arguments.dart';
import '../../widgets/spinner_loader.dart';
import '../../services/database.dart';
import './pet_form_screen.dart';

class ShowBusinessPetsScreen extends StatelessWidget {
  static String routeName = '/business_pets';
  final BaseDatabase database = Database();
  final String placeholderImage = PlaceholderImages.placeholderFiru;

  List<DataRow> _buildList(BuildContext context, List<DocumentSnapshot> snapshot, Business business) {
    return snapshot.map((data) => _buildListItem(context, data, business)).toList();
  }

  DataRow _buildListItem(BuildContext context, DocumentSnapshot data, Business business) {
    Pet pet = Pet.buildPet(data);
    return DataRow(cells: [
      DataCell(Text(pet.name)),
      DataCell(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(tooltip: 'Mostrar', icon: Icon(Icons.remove_red_eye), onPressed: () => _openShowFullData(context, pet, business)),
            IconButton(tooltip: 'Editar', icon: Icon(Icons.edit), onPressed: () => _openEditForm(context, pet, business)),
            IconButton(tooltip: 'Eliminar', icon: Icon(Icons.delete), onPressed: () => _checkDelete(context, pet.id, pet.pictureUrl))
          ]
        )
      )
    ]);
  }

  void _openNewForm(BuildContext context, Business business) {
    Navigator.of(context).pushNamed(PetFormScreen.routeName, arguments: PetArguments(null, business.id, business.name));
  }

  void _openShowFullData(BuildContext context, Pet pet, Business business) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          // useMaterialBorderRadius: false,
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Información de Firu\n\n', style: TextStyle(fontSize: 16, color: CustomColors.mainDark, fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'pet_${pet.id}',
                  child: FadeInImage(
                    height: 250,
                    image: pet.pictureUrl == null ? Image.asset(placeholderImage) : NetworkImage(pet.pictureUrl),
                    placeholder: AssetImage(placeholderImage),
                    fit: BoxFit.contain,
                  ),
                ),
                Padding(padding: EdgeInsets.all(15)),
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                    style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
                    children: <TextSpan>[
                      TextSpan(text: 'Nombre: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.name}\n\n'),
                      TextSpan(text: 'Género: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.sex ? 'Macho':'Hembra'}\n\n'),
                      TextSpan(text: 'Nacimiento: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.age}\n\n'),
                      TextSpan(text: 'Descripción: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${pet.description}')
                    ]
                  )
                )
              ]
            ),
          ),
          actions: [
            FlatButton(
              child: Text('EDITAR', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                _openEditForm(context, pet, business);
              }
            ),
            FlatButton(
              child: Text('CERRAR', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _openEditForm(BuildContext context, Pet pet, Business business) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡AVISO!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed(PetFormScreen.routeName, arguments: PetArguments(pet, business.id, business.name));
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _checkDelete(BuildContext context, String petId, String petPictureUrl) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea eliminar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                database.deletePet(petId, petPictureUrl);
                Navigator.of(context).pop();
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = true;
    final Business _business = ModalRoute.of(context).settings.arguments;
    if(_business.id != null) {
      _isLoading = false;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: _isLoading ? SpinnerLoader() : Container(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(20)),
            Container(
              color:  CustomColors.mainGreen,
              child: IconButton(
                icon: Icon(Icons.add, color: Colors.white),
                onPressed: () => _openNewForm(context, _business),
                tooltip: 'Agregar Nuevo Firu'
              )
            ),
            StreamBuilder(
              stream: Firestore.instance.collection('pet').where('businessId', isEqualTo: _business.id).orderBy('name', descending: false).snapshots(),
              builder: (context, snapshot) {
                switch(snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return SpinnerLoader();
                  default:
                    return Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      alignment: Alignment.topCenter,
                      child: DataTable(
                        columns: [
                          DataColumn(label: Text('Nombre')),
                          DataColumn(label: Text('Acciones'))
                        ],
                        rows: _buildList(context, snapshot.data.documents, _business)
                      )
                    );
                }
              }
            )
          ]
        )
      )
    );
  }
}
