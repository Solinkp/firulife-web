import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/address_arguments.dart';
import '../../widgets/address_info_form.dart';

class AddressInfoFormScreen extends StatelessWidget {
  static String routeName = '/address_info_form';

  @override
  Widget build(BuildContext context) {
    final AddressArguments _args = ModalRoute.of(context).settings.arguments;
    final Business _business = _args.business;
    final int _index = _args.index;
    final String _title = _index == null ? 'Nuevo' : 'Editar';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$_title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: AddressInfoForm(_business, _index)
    );
  }
}
