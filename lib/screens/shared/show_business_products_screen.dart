import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/business.dart';
import '../../models/product.dart';
import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';
import '../../settings/product_arguments.dart';
import '../../widgets/spinner_loader.dart';
import '../../services/database.dart';
import './product_form_screen.dart';

class ShowBusinessProductsScreen extends StatelessWidget {
  static String routeName = '/business_products';
  final BaseDatabase database = Database();
  final String placeholderImage = PlaceholderImages.placeholderProduct;

  List<DataRow> _buildList(BuildContext context, List<DocumentSnapshot> snapshot, Business business) {
    return snapshot.map((data) => _buildListItem(context, data, business)).toList();
  }

  DataRow _buildListItem(BuildContext context, DocumentSnapshot data, Business business) {
    Product product = Product.buildProduct(data);
    return DataRow(cells: [
      DataCell(Text(product.name)),
      DataCell(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(tooltip: 'Mostrar', icon: Icon(Icons.remove_red_eye), onPressed: () => _openShowFullData(context, product, business)),
            IconButton(tooltip: 'Editar', icon: Icon(Icons.edit), onPressed: () => _openEditForm(context, product, business)),
            IconButton(tooltip: 'Eliminar', icon: Icon(Icons.delete), onPressed: () => _checkDelete(context, product.id, business.type, product.pictureUrl))
          ]
        )
      )
    ]);
  }

  void _openNewForm(BuildContext context, Business business) {
    Navigator.of(context).pushNamed(ProductFormScreen.routeName, arguments: ProductArguments(null, business.id, business.name, business.type));
  }

  void _openShowFullData(BuildContext context, Product product, Business business) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          // useMaterialBorderRadius: false,
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Información de Producto\n\n', style: TextStyle(fontSize: 16, color: CustomColors.mainDark, fontWeight: FontWeight.bold)),
                Hero(
                  tag: 'product_${product.id}',
                  child: FadeInImage(
                    height: 250,
                    image: product.pictureUrl == null ? Image.asset(placeholderImage) : NetworkImage(product.pictureUrl),
                    placeholder: AssetImage(placeholderImage),
                    fit: BoxFit.contain
                  )
                ),
                Padding(padding: EdgeInsets.all(15)),
                RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                    style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
                    children: <TextSpan>[
                      TextSpan(text: 'Nombre: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.name}\n\n'),
                      TextSpan(text: 'Precio: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.currency}${product.price}\n\n'),
                      TextSpan(text: 'Descripción Corta: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.smallDescription}\n\n'),
                      TextSpan(text: 'Descripción Completa: ', style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: '${product.fullDescription}')
                    ]
                  )
                )
              ]
            ),
          ),
          actions: [
            FlatButton(
              child: Text('EDITAR', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                _openEditForm(context, product, business);
              }
            ),
            FlatButton(
              child: Text('CERRAR', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _openEditForm(BuildContext context, Product product, Business business) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡AVISO!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed(ProductFormScreen.routeName, arguments: ProductArguments(product, business.id, business.name, business.type));
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _checkDelete(BuildContext context, String productId, int businessType, String productPictureUrl) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea eliminar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                database.deleteProduct(businessType, productId, productPictureUrl);
                Navigator.of(context).pop();
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = true;
    final Business _business = ModalRoute.of(context).settings.arguments;
    if(_business.id != null) {
      _isLoading = false;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: _isLoading ? SpinnerLoader() : Container(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(20)),
            Container(
              color:  CustomColors.mainGreen,
              child: IconButton(
                icon: Icon(Icons.add, color: Colors.white),
                onPressed: () => _openNewForm(context, _business),
                tooltip: 'Agregar Nuevo Producto'
              )
            ),
            StreamBuilder(
              stream: Firestore.instance.collection('product').where('businessId', isEqualTo: _business.id).orderBy('name', descending: false).snapshots(),
              builder: (context, snapshot) {
                switch(snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return SpinnerLoader();
                  default:
                    return Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      alignment: Alignment.topCenter,
                      child: DataTable(
                        columns: [
                          DataColumn(label: Text('Nombre')),
                          DataColumn(label: Text('Acciones'))
                        ],
                        rows: _buildList(context, snapshot.data.documents, _business)
                      )
                    );
                }
              }
            )
          ]
        )
      )
    );
  }
}
