import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/phone_arguments.dart';
import '../../widgets/phone_info_form.dart';

class PhoneInfoFormScreen extends StatelessWidget {
  static String routeName = '/phone_info_form';

  @override
  Widget build(BuildContext context) {
    final PhoneArguments _args = ModalRoute.of(context).settings.arguments;
    final Business _business = _args.business;
    final int _index = _args.index;
    final String _title = _index == null ? 'Nuevo' : 'Editar';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$_title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: PhoneInfoForm(_business, _index)
    );
  }
}
