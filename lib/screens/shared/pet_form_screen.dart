import 'package:flutter/material.dart';

import '../../models/pet.dart';
import '../../settings/custom_colors.dart';
import '../../settings/pet_arguments.dart';
import '../../widgets/pet_form.dart';

class PetFormScreen extends StatelessWidget {
  static String routeName = '/pet_form';

  @override
  Widget build(BuildContext context) {
    final PetArguments _args = ModalRoute.of(context).settings.arguments;
    final Pet _pet = _args.pet;
    final String _businessId = _args.businessId;
    final String _businessName = _args.businessName;

    final String title = _pet == null ? 'Nuevo' : 'Editar';
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$title Firu', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: PetForm(_pet, _businessId, _businessName)
    );
  }
}
