import 'package:flutter/material.dart';

import '../../settings/custom_colors.dart';
import '../../settings/new_business_arguments.dart';
import '../../widgets/new_business_form.dart';

class NewBusinessFormScreen extends StatelessWidget {
  static String routeName = '/new_business_form';

  @override
  Widget build(BuildContext context) {
    final NewBusinessArguments _args = ModalRoute.of(context).settings.arguments;
    final VoidCallback _newBusinessCreatedCallback = _args.newBusinessCreatedCallback;
    final String _userId = _args.userId;
    final String _title = 'Nuevo Negocio';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$_title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: NewBusinessForm(_newBusinessCreatedCallback, _userId)
    );
  }
}
