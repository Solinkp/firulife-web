import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/donation_account_key_arguments.dart';
import '../../widgets/donation_account_key_form.dart';

class DonationAccountKeyFormScreen extends StatelessWidget {
  static String routeName = '/donation_account_key_form';

  @override
  Widget build(BuildContext context) {
    final DonationAccountKeyArguments _args = ModalRoute.of(context).settings.arguments;
    final Business _business = _args.business;
    final String _mapKey = _args.mapKey;
    final String _title = _mapKey == null ? 'Nuevo Banco/Servicio' : 'Editar Banco/Servicio';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$_title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: DonationAccountKeyForm(_business, _mapKey)
    );
  }
}
