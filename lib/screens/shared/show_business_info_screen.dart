import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/info_panel.dart';
import '../../services/database.dart';

class ShowBusinessInfoScreen extends StatelessWidget {
  static String routeName = '/business_info';
  final BaseDatabase database = Database();
  final String placeholderImage = PlaceholderImages.placeholderBanner;

  Widget _buildInfoPanel(String title, int type, Business business) {
    return InfoPanel(title: title, type: type, business: business);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    bool _isLoading = true;
    final Business _business = ModalRoute.of(context).settings.arguments;
    if(_business.id != null) {
      _isLoading = false;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen,
      ),
      body: _isLoading ? SpinnerLoader() : StreamBuilder(
        stream: Firestore.instance.collection('business').document(_business.id).snapshots(),
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return SpinnerLoader();
            default:
              Business businessBuilt = Business.buildBusiness(snapshot.data);
              return Container(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Información Básica', 0, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Horarios de Atención', 1, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Redes de Contacto', 2, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Direcciones', 3, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Teléfonos', 4, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15)),
                      _buildInfoPanel('Servicios', 5, businessBuilt),
                      Padding(padding: EdgeInsets.all(size.width <= 550 ? 7 : 15))
                    ]
                  )
                )
              );
          }
        }
      )
    );
  }
}
