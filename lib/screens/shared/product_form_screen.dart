import 'package:flutter/material.dart';

import '../../models/product.dart';
import '../../settings/custom_colors.dart';
import '../../settings/product_arguments.dart';
import '../../widgets/product_form.dart';

class ProductFormScreen extends StatelessWidget {
  static String routeName = '/product_form';

  @override
  Widget build(BuildContext context) {
    final ProductArguments _args = ModalRoute.of(context).settings.arguments;
    final Product _product = _args.product;
    final String _businessId = _args.businessId;
    final String _businessName = _args.businessName;
    final int _businessType = _args.businessType;

    final String title = _product == null ? 'Nuevo' : 'Editar';
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$title Producto', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: ProductForm(_product, _businessId, _businessName, _businessType)
    );
  }
}
