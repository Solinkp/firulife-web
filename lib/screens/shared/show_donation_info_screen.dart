import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/donation_account_key_arguments.dart';
import '../../settings/donation_account_detail_arguments.dart';
import '../../widgets/spinner_loader.dart';
import '../../services/database.dart';
import './donation_account_key_form_screen.dart';
import './donation_account_detail_screen.dart';

class ShowDonationInfoScreen extends StatelessWidget {
  static String routeName = '/donation_info';
  final BaseDatabase database = Database();

  List<DataRow> _buildList(BuildContext context, Business business) {
    List donationKeys = business.donationAccounts == null ? [] : business.donationAccounts.keys.toList(); //nombres cuentas
    return donationKeys.map((key) => _buildListItem(context, key, business)).toList();
  }

  DataRow _buildListItem(BuildContext context, String mapKey, Business business) {
    return DataRow(cells: [
      DataCell(Text(mapKey)),
      DataCell(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(tooltip: 'Ver Cuentas Asociadas', icon: Icon(Icons.remove_red_eye), onPressed: () => _openDonationAccountDetails(context, business, mapKey)),
            IconButton(tooltip: 'Editar Nombre de Banco/Servicio', icon: Icon(Icons.edit), onPressed: () => _editItem(context, business, mapKey)),
            IconButton(tooltip: 'Eliminar Banco/Servicio', icon: Icon(Icons.delete), onPressed: () => _deleteItem(context, business, mapKey))
          ]
        )
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = true;
    final Business _business = ModalRoute.of(context).settings.arguments;
    if(_business.id != null) {
      _isLoading = false;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: _isLoading ? SpinnerLoader() : StreamBuilder(
        stream: Firestore.instance.collection('business').document(_business.id).snapshots(),
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return SpinnerLoader();
            default:
              Business businessBuilt = Business.buildBusiness(snapshot.data);
              return Container(
                child: Column(
                  children: [
                    Padding(padding: EdgeInsets.all(20)),
                    Container(
                      color:  CustomColors.mainGreen,
                      child: IconButton(
                        icon: Icon(Icons.add, color: Colors.white),
                        onPressed: () => {
                          Navigator.of(context).pushNamed(DonationAccountKeyFormScreen.routeName, arguments: DonationAccountKeyArguments(businessBuilt, null))
                        },
                        tooltip: 'Agregar Nuevo Banco / Servicio En Línea'
                      )
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      alignment: Alignment.topCenter,
                      child: DataTable(
                        columns: [
                          DataColumn(label: Text('Banco / Servicio')),
                          DataColumn(label: Text('Acciones'))
                        ],
                        rows: _buildList(context, businessBuilt)
                      )
                    ),
                  ],
                ),
              );
          }
        }
      )
    );
  }

  void _openDonationAccountDetails(BuildContext context, Business business, String mapKey) {
    Navigator.of(context).pushNamed(DonationAccountDetailScreen.routeName, arguments: DonationAccountDetailArguments(business, mapKey, null));
  }

  void _editItem(BuildContext context, Business business, String mapKey) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed(DonationAccountKeyFormScreen.routeName, arguments: DonationAccountKeyArguments(business, mapKey));
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, Business business, String mapKey) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea eliminar el registro?\n\n'),
                TextSpan(text: 'Se eliminarán todas las cuentas asociadas a este banco/servicio.')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () async {
                Map donationAccounts = business.donationAccounts.cast();
                donationAccounts.remove(mapKey);
                String docId = await database.editBusinessInfo(business);
                if(docId != null) {
                  Navigator.of(context).pop();
                }
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}
