import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/donation_account_detail_arguments.dart';
import '../../widgets/spinner_loader.dart';
import '../../services/database.dart';
import './donation_account_detail_form_screen.dart';

class DonationAccountDetailScreen extends StatelessWidget {
  static String routeName = '/donation_account_detail';
  final BaseDatabase database = Database();

  List<DataRow> _buildList(BuildContext context, Business business, String mapKey) {
    List accounts = business.donationAccounts.cast()[mapKey]; // cuentas
    List<DataRow> rows = [];
    for(int i = 0; i < accounts.length; i ++) {
      rows.add(_buildListItem(context, accounts[i], i, business, mapKey));
    }
    return rows;
  }

  DataRow _buildListItem(BuildContext context, Map map, int index, Business business, String mapKey) {
    return DataRow(cells: [
      DataCell(Text('${map.keys.first.toString()}: ${map.values.first.toString()}')),
      DataCell(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(tooltip: 'Editar Cuenta', icon: Icon(Icons.edit), onPressed: () => _editItem(context, business, mapKey, index)),
            IconButton(tooltip: 'Eliminar Cuenta', icon: Icon(Icons.delete), onPressed: () => _deleteItem(context, business, mapKey, index))
          ]
        )
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = true;
    final DonationAccountDetailArguments _args = ModalRoute.of(context).settings.arguments;
    final Business _business = _args.business;
    final String _mapKey = _args.mapKey;
    if(_business.id != null) {
      _isLoading = false;
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: _isLoading ? SpinnerLoader() : StreamBuilder(
        stream: Firestore.instance.collection('business').document(_business.id).snapshots(),
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return SpinnerLoader();
            default:
              Business businessBuilt = Business.buildBusiness(snapshot.data);
              return Container(
                child: Column(
                  children: [
                    Padding(padding: EdgeInsets.all(20)),
                    Container(
                      color:  CustomColors.mainGreen,
                      child: IconButton(
                        icon: Icon(Icons.add, color: Colors.white),
                        onPressed: () => {
                          Navigator.of(context).pushNamed(DonationAccountDetailFormScreen.routeName, arguments: DonationAccountDetailArguments(businessBuilt, _mapKey, null))
                        },
                        tooltip: 'Agregar Nueva Cuenta'
                      )
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      alignment: Alignment.topCenter,
                      child: DataTable(
                        columns: [
                          DataColumn(label: Text('Cuenta')),
                          DataColumn(label: Text('Acciones'))
                        ],
                        rows: _buildList(context, businessBuilt, _mapKey)
                      )
                    ),
                  ],
                ),
              );
          }
        }
      )
    );
  }

  void _editItem(BuildContext context, Business business, String mapKey, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed(DonationAccountDetailFormScreen.routeName, arguments: DonationAccountDetailArguments(business, mapKey, index));
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, Business business, String mapKey, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea eliminar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () async {
                Map donationAccounts = business.donationAccounts.cast();
                List accounts = donationAccounts[mapKey];
                accounts.removeAt(index);
                String docId = await database.editBusinessInfo(business);
                if(docId != null) {
                  Navigator.of(context).pop();
                }
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}
