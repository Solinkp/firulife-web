import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../widgets/schedule_info_form.dart';

class ScheduleInfoFormScreen extends StatelessWidget {
  static String routeName = '/schedule_info_form';

  @override
  Widget build(BuildContext context) {
    Business _business = ModalRoute.of(context).settings.arguments;
    final String title = 'Editar Horarios de Atención';

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('$title', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen
      ),
      body: ScheduleInfoForm(_business)
    );
  }
}
