import 'package:flutter/material.dart';

import '../../models/business.dart';
import '../../settings/custom_colors.dart';
import '../../settings/new_business_arguments.dart';
import '../../settings/pop_up_menu_options.dart';
import '../../settings/custom_snackbar.dart';
import '../../services/authentication.dart';
import '../../services/database.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_segment_business.dart';

class BusinessHomeScreen extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final String displayName;

  BusinessHomeScreen({this.auth, this.userId, this.logoutCallback, this.displayName});

  @override
  _BusinessHomeScreenState createState() => _BusinessHomeScreenState();
}

class _BusinessHomeScreenState extends State<BusinessHomeScreen> {
  final BaseDatabase database = Database();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Business _business;
  bool _isLoading;
  var size;

  @override
  void initState() {
    _isLoading = true;
    database.getLinkedBusiness(widget.userId).then((data) {
      setState(() {
        if(data != null) {
          _business = Business.buildBusiness(data);
          _isLoading = false;
        } else {
          _isLoading = false;
        }
      });
    });
    super.initState();
  }

  void newBusinessCreatedCallback() {
    _isLoading = true;
    database.getLinkedBusiness(widget.userId).then((data) {
      setState(() {
        if(data != null) {
          _business = Business.buildBusiness(data);
          _isLoading = false;
        } else {
          _isLoading = false;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen,
        actions: [
          PopupMenuButton(
            tooltip: "Mostrar Menú",
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return PopUpMenuOptions.options.map((String option) {
                return PopupMenuItem(
                  value: option,
                  child: Text(option)
                );
              }).toList();
            }
          )
        ]
      ),
      body: _isLoading ? _showLoader() : _business == null ? _showContentNoData(context) : _showContentData()
    );
  }

  void choiceAction(String choice) {
    if(choice == PopUpMenuOptions.password) {
      widget.auth.getCurrentUser().then((user) {
        if (user != null) {
          String email = user['user'].email;
          widget.auth.sendPasswordResetEmail(email);
          Widget snackBar = CustomSnackbar(message: "Te hemos enviado un mensaje a tu correo para que puedas cambiar tu contraseña.").build(context);
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      });
    }
    if(choice == PopUpMenuOptions.signOut) {
      signOut();
    }
  }

  Widget _showLoader() {
    return Center(child: SpinnerLoader());
  }

  Widget _showContentNoData(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        height: 250,
        width: 300,
        child: FiruSegmentBusiness(null, 'Crear Registro de Negocio', 4, NewBusinessArguments(newBusinessCreatedCallback, widget.userId))
      ),
    );
  }

  Widget _showContentData() {
    final double itemHeight = (size.height - kToolbarHeight - 24) / 3;
    final double itemWidth = size.width / 2;
    String infoText = "Ver Información";
    String productText = "Ver Productos";
    String firusText = "Ver Peluditos en adopción";
    String donationText = "Administrar cuentas de donaciones";

    return Container(
      child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Center(child: Text('Bienvenido Firu Usuario ${widget.displayName}', style: Theme.of(context).textTheme.bodyText2, textAlign: TextAlign.center)),
        ),
        GridView.count(
          childAspectRatio: (itemWidth / itemHeight),
          crossAxisCount: 2,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
          shrinkWrap: true,
          children: [
            // Info
            FiruSegmentBusiness(_business, infoText, 0, null),
            // Productos
            FiruSegmentBusiness(_business, productText, 1, null),
            // Firus
            _business.type == 0 ? FiruSegmentBusiness(_business, firusText, 2, null) : Container(),
            // DonationAcc
            _business.type == 0 ? FiruSegmentBusiness(_business, donationText, 3, null) : Container()
          ]
        )
      ])
    );
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }
}
