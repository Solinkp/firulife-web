import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';
import '../services/authentication.dart';

class NotAllowedScreen extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback logoutCallback;

  NotAllowedScreen({this.auth, this.logoutCallback});

  @override
  _NotAllowedScreenState createState() => _NotAllowedScreenState();
}

class _NotAllowedScreenState extends State<NotAllowedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen,
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text('No tienes permiso para acceder a este espacio.', style: Theme.of(context).textTheme.bodyText2)
            ),
            Padding(
              padding: EdgeInsets.all(20),
            ),
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: signOut,
              tooltip: 'Cerrar Sesión'
            )
          ]
        ),
      )
    );
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }
}
