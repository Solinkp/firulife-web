import 'package:flutter/material.dart';

import '../../settings/custom_colors.dart';
import '../../settings/custom_snackbar.dart';
import '../../services/authentication.dart';

class AdminHomeScreen extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final String displayName;

  AdminHomeScreen({this.auth, this.userId, this.logoutCallback, this.displayName});

  @override
  _AdminHomeScreenState createState() => _AdminHomeScreenState();
}

class _AdminHomeScreenState extends State<AdminHomeScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _email;
  String _password;
  String _displayName;
  bool _admin = false;
  String _errorMessage;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
    });
    if (validateAndSave()) {
      String userId = "";
      Widget snackBar;
      try {
        userId = await widget.auth.signUp(_email, _password, _displayName, _admin);

        if (userId.length > 0 && userId != null) {
          snackBar = CustomSnackbar(message: "Usuario $_displayName creado satisfactoriamente").build(context);
        } else {
          snackBar = CustomSnackbar(message: "Usuario $_displayName no creado, verifique e intente de nuevo.").build(context);
        }
        _scaffoldKey.currentState.showSnackBar(snackBar);
        resetForm();
      } catch (e) {
        print('Error: $e');
        setState(() {
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Firulife Web Admin - User ${widget.displayName}', style: Theme.of(context).textTheme.headline6),
        backgroundColor: CustomColors.mainGreen,
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: signOut,
            tooltip: 'Cerrar Sesión'
          )
        ]
      ),
      body: _showForm()
    );
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  Widget _showForm() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showEmailInput(),
            showPasswordInput(),
            showNameInput(),
            showAdminInput(),
            showPrimaryButton(),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Correo',
          icon: Icon(
            Icons.mail,
            color: Colors.grey
          )
        ),
        validator: (value) => value.isEmpty ? 'Correo no puede estar vacío' : null,
        onSaved: (value) => _email = value.trim()
      )
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Contraseña',
          icon: Icon(
            Icons.lock,
            color: Colors.grey
          )
        ),
        validator: (value) => value.isEmpty ? 'Contraseña no puede estar vacía' : null,
        onSaved: (value) => _password = value.trim()
      )
    );
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre',
          icon: Icon(
            Icons.text_fields,
            color: Colors.grey
          )
        ),
        validator: (value) => value.isEmpty ? 'Nombre no puede estar vacío' : null,
        onSaved: (value) => _displayName = value.trim()
      )
    );
  }

  Widget showAdminInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: CheckboxListTile(
        // autofocus: false,
        value: _admin,
        title: Text('Admin'),
        onChanged: (value) {
          if(!_admin) {
            setState(() {
              _admin = true;
            });
          } else {
            setState(() {
              _admin = false;
            });
          }
        }
      )
    );
  }

  Widget showPrimaryButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('CREAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(),
        )
      )
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return new Container(
        height: 0.0
      );
    }
  }

}
