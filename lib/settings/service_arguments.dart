import '../models/business.dart';

class ServiceArguments {
  final Business business;
  final int index;

  ServiceArguments(this.business, this.index);
}
