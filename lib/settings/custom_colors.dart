import 'package:flutter/material.dart';

class CustomColors {
  static const mainGreen = Color(0xFF45B4A8);
  static const mainDark = Color(0XFF3E6866);
}
