import '../models/business.dart';

class PhoneArguments {
  final Business business;
  final int index;

  PhoneArguments(this.business, this.index);
}
