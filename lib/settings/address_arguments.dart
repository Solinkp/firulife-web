import '../models/business.dart';

class AddressArguments {
  final Business business;
  final int index;

  AddressArguments(this.business, this.index);
}
