import '../models/pet.dart';

class PetArguments {
  final Pet pet;
  final String businessId;
  final String businessName;

  PetArguments(this.pet, this.businessId, this.businessName);
}
