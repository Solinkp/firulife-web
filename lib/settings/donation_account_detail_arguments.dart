import '../models/business.dart';

class DonationAccountDetailArguments {
  final Business business;
  final String mapKey;
  final int index;

  DonationAccountDetailArguments(this.business, this.mapKey, this.index);
}
