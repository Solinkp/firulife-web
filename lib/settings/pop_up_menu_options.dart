class PopUpMenuOptions {
  static const String password = "Cambiar Contraseña";
  static const String signOut = "Cerrar Sesión";

  static const List<String> options = [
    password,
    signOut
  ];
}
