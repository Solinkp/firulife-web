import '../models/business.dart';

class DonationAccountKeyArguments {
  final Business business;
  final String mapKey;

  DonationAccountKeyArguments(this.business, this.mapKey);
}
