import 'package:flutter/material.dart';

class NewBusinessArguments {
  final VoidCallback newBusinessCreatedCallback;
  final String userId;

  NewBusinessArguments(this.newBusinessCreatedCallback, this.userId);
}
