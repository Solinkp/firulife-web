import '../models/product.dart';

class ProductArguments {
  final Product product;
  final String businessId;
  final String businessName;
  final int businessType;

  ProductArguments(this.product, this.businessId, this.businessName, this.businessType);
}
