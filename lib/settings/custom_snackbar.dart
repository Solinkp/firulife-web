import 'package:flutter/material.dart';

import './custom_colors.dart';

class CustomSnackbar extends StatelessWidget {
  final String message;

  CustomSnackbar({@required this.message});

  @override
  Widget build(BuildContext context) {
    return SnackBar(
      backgroundColor: CustomColors.mainDark,
      content: Text(message),
      action: SnackBarAction(
        label: 'X',
        textColor: Colors.white,
        onPressed: () {
          Scaffold.of(context).hideCurrentSnackBar();
        }
      ),
    );
  }
}
