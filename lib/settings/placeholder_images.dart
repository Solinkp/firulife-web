class PlaceholderImages {
  static const placeholderBanner = 'assets/images/placeholder_banner_no_bck.png';
  static const placeholderFiru = 'assets/images/placeholder_firu.png';
  static const placeholderProduct = 'assets/images/placeholder_products.png';
}
