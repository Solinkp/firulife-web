import 'package:flutter/material.dart';

import './settings/custom_colors.dart';
import './screens/root_screen.dart';
import './services/authentication.dart';
// Admin

// Business
import './screens/shared/show_business_info_screen.dart';
import './screens/shared/show_business_products_screen.dart';
import './screens/shared/show_business_pets_screen.dart';
import './screens/shared/general_info_form_screen.dart';
import './screens/shared/schedule_info_form_screen.dart';
import './screens/shared/contact_info_form_screen.dart';
import './screens/shared/address_info_form_screen.dart';
import './screens/shared/phone_info_form_screen.dart';
import './screens/shared/service_info_form_screen.dart';
import './screens/shared/product_form_screen.dart';
import './screens/shared/pet_form_screen.dart';
import './screens/shared/new_business_form_screen.dart';
import './screens/shared/show_donation_info_screen.dart';
import './screens/shared/donation_account_key_form_screen.dart';
import './screens/shared/donation_account_detail_screen.dart';
import './screens/shared/donation_account_detail_form_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Firulife',
      theme: ThemeData(
        accentColor: CustomColors.mainGreen,
        textTheme: ThemeData.light().textTheme.copyWith(
          headline6: TextStyle(fontSize: 26, color: Colors.white, fontFamily: 'Courgette'),
          headline5: TextStyle(fontSize: 23, color: Colors.white, fontFamily: 'Courgette'),
          headline4: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Courgette'),
          subtitle2: TextStyle(fontSize: 20, color: CustomColors.mainDark, fontFamily: 'Courgette'),
          bodyText2: TextStyle(fontSize: 20, color: CustomColors.mainDark),
          bodyText1: TextStyle(fontSize: 18, color: Colors.white),
        ),
        fontFamily: 'Quicksand'
      ),
      home: RootScreen(auth: Auth()),
      routes: {
        ShowBusinessInfoScreen.routeName: (_) => ShowBusinessInfoScreen(),
        ShowBusinessProductsScreen.routeName: (_) => ShowBusinessProductsScreen(),
        ShowBusinessPetsScreen.routeName: (_) => ShowBusinessPetsScreen(),
        GeneralInfoFormScreen.routeName: (_) => GeneralInfoFormScreen(),
        ScheduleInfoFormScreen.routeName: (_) => ScheduleInfoFormScreen(),
        ContactInfoFormScreen.routeName: (_) => ContactInfoFormScreen(),
        AddressInfoFormScreen.routeName: (_) => AddressInfoFormScreen(),
        PhoneInfoFormScreen.routeName: (_) => PhoneInfoFormScreen(),
        ServiceInfoFormScreen.routeName: (_) => ServiceInfoFormScreen(),
        ProductFormScreen.routeName: (_) => ProductFormScreen(),
        PetFormScreen.routeName: (_) => PetFormScreen(),
        NewBusinessFormScreen.routeName: (_) => NewBusinessFormScreen(),
        ShowDonationInfoScreen.routeName: (_) => ShowDonationInfoScreen(),
        DonationAccountKeyFormScreen.routeName: (_) => DonationAccountKeyFormScreen(),
        DonationAccountDetailScreen.routeName: (_) => DonationAccountDetailScreen(),
        DonationAccountDetailFormScreen.routeName: (_) => DonationAccountDetailFormScreen()
      },
      debugShowCheckedModeBanner: false
    );
  }
}
