import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_functions/cloud_functions.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password, String displayName, bool admin);

  Future<dynamic> getCurrentUser();

  Future<void> signOut();

  Future<void> sendPasswordResetEmail(String email);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final HttpsCallable _createFirulifeUser = CloudFunctions.instance.getHttpsCallable(functionName: 'createFirulifeUser');

  Future<String> signIn(String email, String password) async {
    AuthResult result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future<String> signUp(String email, String password, String displayName, bool admin) async {
    int type = admin == true ? 0 : 1;
    var result = await _createFirulifeUser.call(<String, dynamic>{
      'email': email,
      'password': password,
      'displayName': displayName,
      'type': type
    });
    return result.data['uid'].toString().trim();
  }

  Future<dynamic> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    int userType;
    if(user != null) {
      await user.getIdToken().then((idTokenResult) => {
        userType = idTokenResult.claims['type'],
      });
      return {'user': user, 'type': userType};
    }
    return null;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendPasswordResetEmail(String email) async {
    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }
}
