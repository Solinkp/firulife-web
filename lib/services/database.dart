import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/firebase.dart' as fb;

import '../models/business.dart';
import '../models/product.dart';
import '../models/pet.dart';

abstract class BaseDatabase {
  Future<DocumentSnapshot> getLinkedBusiness(String userId);

  // Businesses
  // Business
  Future<String> addNewBusiness(Business business);
  Future<String> uploadBusinessImage(
    int businessType,
    String businessName,
    String fileName,
    dynamic businessImage,
    String mimeType
  );
  Future<String> editBusinessInfo(Business business);
  // Products
  Future<String> uploadProductImage(
    int businessType,
    String businessName,
    String fileName,
    dynamic productImage,
    String mimeType
  );
  Future<String> createProduct(Product product);
  Future<String> editProduct(Product product);
  void deleteProduct(int businessType, String productId, String pictureUrl);

  // Pets
  Future<String> uploadPetImage(
    String businessName,
    String fileName,
    dynamic productImage,
    String mimeType
  );
  Future<String> createPet(Pet pet);
  Future<String> editPet(Pet pet);
  void deletePet(String petId, String pictureUrl);
}

class Database implements BaseDatabase {
  final Firestore _firestore = Firestore.instance;

  Future<DocumentSnapshot> getLinkedBusiness(String userId) async {
    DocumentSnapshot document;
    await _firestore.collection('business').where('userId', isEqualTo: userId).getDocuments().then((event) {
      if (event.documents.isNotEmpty) {
        document = event.documents.single;
      }
    }).catchError((err) => print(err));
    return document;
  }

  // BUSINESS DATABASE STUFF

  Future<String> addNewBusiness(Business business) async {
    try {
      await Firestore.instance.collection('business').add(business.toMapNewBusiness());
      return 'Registro creado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<String> uploadBusinessImage(
    int businessType,
    String businessName,
    String fileName,
    dynamic businessImage,
    String mimeType
  ) async {
    String path = "";
    String businessFolder = businessName.replaceAll(' ', '_');
    switch(businessType) {
      case 0:
        path = "Shelters/$businessFolder/$fileName";
      break;
      case 1:
        path = "Vets/$businessFolder/$fileName";
      break;
      case 2:
        path = "Stores/$businessFolder/$fileName";
      break;
    }
    fb.StorageReference storageReference = fb.storage().ref(path);
    fb.UploadTask uploadTask = storageReference.put(businessImage, fb.UploadMetadata(contentType: mimeType));
    await uploadTask.future;
    String imageUrl;
    await uploadTask.snapshot.ref.getDownloadURL().then((fileURL) {
      imageUrl = fileURL.toString();
    });
    return imageUrl;
  }

  Future<String> editBusinessInfo(Business business) async {
    try {
      await Firestore.instance.collection('business').document(business.id).updateData(business.toMap());
      return 'Registro editado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // PRODUCT DATABASE STUFF

  Future<String> uploadProductImage(
    int businessType,
    String businessName,
    String fileName,
    dynamic productImage,
    String mimeType
  ) async {
    String path = "";
    String businessFolder = businessName.replaceAll(' ', '_');
    switch(businessType) {
      case 0:
        path = "Shelters/$businessFolder/Products/$fileName";
      break;
      case 1:
        path = "Vets/$businessFolder/Products/$fileName";
      break;
      case 2:
        path = "Stores/$businessFolder/Products/$fileName";
      break;
    }
    fb.StorageReference storageReference = fb.storage().ref(path);
    fb.UploadTask uploadTask = storageReference.put(productImage, fb.UploadMetadata(contentType: mimeType));
    await uploadTask.future;
    String imageUrl;
    await uploadTask.snapshot.ref.getDownloadURL().then((fileURL) {
      imageUrl = fileURL.toString();
    });
    return imageUrl;
  }

  Future<String> createProduct(Product product) async {
    try {
      await Firestore.instance.collection('product').add(product.toMap());
      return 'Registro creado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<String> editProduct(Product product) async {
    try {
      await Firestore.instance.collection('product').document(product.id).updateData(product.toMap());
      return 'Registro editado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  void deleteProduct(int businessType, String productId, String pictureUrl) async {
    String path;
    String businessFolder;
    String pictureName = pictureUrl == "" ? null : pictureUrl.split("Products%2F")[1].split("?alt")[0];
    if(pictureName != null) {
      switch(businessType) {
        case 0:
          businessFolder = pictureUrl.split("Shelters%2F")[1].split("%2FProducts")[0];
          path = "Shelters/$businessFolder/Products/$pictureName";
        break;
        case 1:
          businessFolder = pictureUrl.split("Vets%2F")[1].split("%2FProducts")[0];
          path = "Vets/$businessFolder/Products/$pictureName";
        break;
        case 2:
          businessFolder = pictureUrl.split("Stores%2F")[1].split("%2FProducts")[0];
          path = "Stores/$businessFolder/Products/$pictureName";
        break;
      }
    }
    try {
      Firestore.instance.collection('product').document(productId).delete();
      if(path != null) {
        fb.storage().ref(path).delete();
      }
    } catch (e) {
      print(e.toString());
    }
  }

  // PET DATABASE STUFF

  Future<String> uploadPetImage(
    String businessName,
    String fileName,
    dynamic petImage,
    String mimeType
  ) async {
    String businessFolder = businessName.replaceAll(' ', '_');
    String path = "Shelters/$businessFolder/Firus/$fileName";

    fb.StorageReference storageReference = fb.storage().ref(path);
    fb.UploadTask uploadTask = storageReference.put(petImage, fb.UploadMetadata(contentType: mimeType));
    await uploadTask.future;
    String imageUrl;
    await uploadTask.snapshot.ref.getDownloadURL().then((fileURL) {
      imageUrl = fileURL.toString();
    });
    return imageUrl;
  }

  Future<String> createPet(Pet pet) async {
    try {
      await Firestore.instance.collection('pet').add(pet.toMap());
      return 'Registro creado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<String> editPet(Pet pet) async {
    try {
      await Firestore.instance.collection('pet').document(pet.id).updateData(pet.toMap());
      return 'Registro editado exitosamente.';
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  void deletePet(String petId, String pictureUrl) async {
    String path;
    if(pictureUrl != null && pictureUrl.length > 0) {
      String businessFolder = pictureUrl.split("Shelters%2F")[1].split("%2FFirus")[0];
      String pictureName = pictureUrl == "" ? null : pictureUrl.split("Firus%2F")[1].split("?alt")[0];
      path = "Shelters/$businessFolder/Firus/$pictureName";
    }
    try {
      Firestore.instance.collection('pet').document(petId).delete();
      if(path != null) {
        fb.storage().ref(path).delete();
      }
    } catch (e) {
      print(e.toString());
    }
  }

}
