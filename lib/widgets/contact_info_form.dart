import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class ContactInfoForm extends StatefulWidget {
  final Business _business;

  ContactInfoForm(this._business);

  @override
  _ContactInfoFormState createState() => _ContactInfoFormState();
}

class _ContactInfoFormState extends State<ContactInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _contact;
  String _contactWeb;
  String _contactEmail;
  String _contactFacebook;
  String _contactInstagram;
  String _contactTwitter;
  String _contactYoutube;
  String _adoptionLink = "";
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildContact();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        phone: widget._business.phone,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        services: widget._business.services,
        donationAccounts: widget._business.donationAccounts,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        openingHours: widget._business.openingHours,
        // edit
        contact: _contact,
        adoptionLink: _adoptionLink != null || _adoptionLink != "" ? _adoptionLink : ""
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildContact() {
    _contact = [];
    Map<String, String> items = {};
    if(_contactWeb.isNotEmpty) {
      items.putIfAbsent('web', () => _contactWeb);
    }
    if(_contactEmail.isNotEmpty) {
      items.putIfAbsent('email', () => _contactEmail);
    }
    if(_contactFacebook.isNotEmpty) {
      items.putIfAbsent('facebook', () => _contactFacebook);
    }
    if(_contactInstagram.isNotEmpty) {
      items.putIfAbsent('instagram', () => _contactInstagram);
    }
    if(_contactTwitter.isNotEmpty) {
      items.putIfAbsent('twitter', () => _contactTwitter);
    }
    if(_contactYoutube.isNotEmpty) {
      items.putIfAbsent('youtube', () => _contactYoutube);
    }
    _contact.add(items);
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _contact = widget._business.contact.cast<Map<String, String>>();
    List localContact = _contact.cast();
    if(localContact != null && localContact.isNotEmpty) {
      _contactWeb = localContact[0]['web'];
      _contactEmail = localContact[0]['email'];
      _contactFacebook = localContact[0]['facebook'];
      _contactInstagram = localContact[0]['instagram'];
      _contactTwitter = localContact[0]['twitter'];
      _contactYoutube = localContact[0]['youtube'];
    }
    _adoptionLink = widget._business.adoptionLink;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showWebInput(),
            showMailInput(),
            showFacebookInput(),
            showInstagramInput(),
            showTwitterInput(),
            showYoutubeInput(),
            widget._business.type == 0 ? showAdoptionLinkInput() : Container(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

    Widget showWebInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactWeb,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Link de página web (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Página Web'
        ),
        onSaved: (value) => _contactWeb = value.trim()
      )
    );
  }

  Widget showMailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactEmail,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Correo electrónico (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.email,
            color: CustomColors.mainDark
          ),
          helperText: 'Correo'
        ),
        onSaved: (value) => _contactEmail = value.trim()
      )
    );
  }

  Widget showFacebookInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactFacebook,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Link completo de página de Facebook (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Facebook'
        ),
        onSaved: (value) => _contactFacebook = value.trim()
      )
    );
  }

  Widget showInstagramInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactInstagram,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Link completo de cuenta en Instagram (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Instagram'
        ),
        onSaved: (value) => _contactInstagram = value.trim()
      )
    );
  }

  Widget showTwitterInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactTwitter,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Link completo de cuenta en Twitter (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Twitter'
        ),
        onSaved: (value) => _contactTwitter = value.trim()
      )
    );
  }

  Widget showYoutubeInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _contactYoutube,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Link completo de canal de Youtube (Dejar vacío si no tiene)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Youtube'
        ),
        onSaved: (value) => _contactYoutube = value.trim()
      )
    );
  }

  Widget showAdoptionLinkInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _adoptionLink,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Contacto de Adopciones',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Contacto de Adopciones'
        ),
        onSaved: (value) => _adoptionLink = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
