import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path/path.dart' as Path;

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../models/product.dart';
import './spinner_loader.dart';

class ProductForm extends StatefulWidget {
  final Product _product;
  final String _businessId;
  final String _businessName;
  final int _businessType;

  ProductForm(this._product, this._businessId, this._businessName, this._businessType);

  @override
  _ProductFormState createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _currency;
  double _price;
  String _smallDescription;
  String _fullDescription;
  String _pictureUrl = "";
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String docId;
      Product formProduct = Product(
        id: widget._product != null ? widget._product.id : null,
        businessId: widget._businessId,
        name: _name,
        currency: _currency,
        price: _price,
        smallDescription: _smallDescription,
        fullDescription: _fullDescription,
        pictureUrl: _pictureUrl != null || _pictureUrl != "" ? _pictureUrl : ""
      );
      if(widget._product != null) {
        docId = await database.editProduct(formProduct);
      } else {
        docId = await database.createProduct(formProduct);
      }
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    if(widget._product != null) {
      _name = widget._product.name;
      _currency = widget._product.currency;
      _price = widget._product.price;
      _smallDescription = widget._product.smallDescription;
      _fullDescription = widget._product.fullDescription;
      _pictureUrl = widget._product.pictureUrl;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showPictureInput(),
            showNameInput(),
            showCurrencyInput(),
            showPriceInput(),
            showSmallDescriptionInput(),
            showFullDescriptionInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showPictureInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _pictureUrl == null ?
             Image(height: 250, image: AssetImage(PlaceholderImages.placeholderProduct)) :
             FadeInImage(
               height: 250,
               image: NetworkImage(_pictureUrl),
               placeholder: AssetImage(PlaceholderImages.placeholderProduct),
               fit: BoxFit.contain
             ),
            Padding(padding: EdgeInsets.all(5)),
            RaisedButton(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)
              ),
              color: CustomColors.mainGreen,
              child: Text('SUBIR IMAGEN', style: TextStyle(fontSize: 20.0, color: Colors.white)),
              onPressed: () => pickImage()
            )
          ]
        )
      )
    );
  }

  Future pickImage() async {
    var imageFullInfo = await ImagePickerWeb.getImageInfo;
    if (imageFullInfo.data != null) {
      String mimeType = mime(Path.basename(imageFullInfo.fileName));
      String fileUrl = await database.uploadProductImage(
        widget._businessType,
        widget._businessName,
        imageFullInfo.fileName.replaceAll(' ', '_'),
        imageFullInfo.data,
        mimeType
      );
      setState(() {
        _pictureUrl = fileUrl;
      });
    }
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _name,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Nombre no puede estar vacío' : null,
        onSaved: (value) => _name = value.trim()
      )
    );
  }

  Widget showCurrencyInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _currency,
        decoration: InputDecoration(
          labelText: 'Moneda'
        ),
        items: [
          DropdownMenuItem(child: Text('C\$'), value: 'C\$'),
          DropdownMenuItem(child: Text('\$'), value: '\$')
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (_) {},
        onSaved: (value) => _currency = value
      )
    );
  }

  Widget showPriceInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _price == null ? '' : _price.toString(),
        maxLines: 1,
        keyboardType: TextInputType.number,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Precio',
          icon: Icon(
            Icons.attach_money,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Precio no puede estar vacío ni contener letras.' : null,
        onSaved: (value) => _price = num.parse(value.trim())
      )
    );
  }

  Widget showSmallDescriptionInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _smallDescription,
        keyboardType: TextInputType.multiline,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Descripción Corta',
          icon: Icon(
            Icons.description,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Descripción Corta no puede estar vacío' : null,
        onSaved: (value) => _smallDescription = value.trim()
      )
    );
  }

  Widget showFullDescriptionInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _fullDescription,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Descripción Completa',
          icon: Icon(
            Icons.description,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Descripción Completa no puede estar vacío' : null,
        onSaved: (value) => _fullDescription = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
