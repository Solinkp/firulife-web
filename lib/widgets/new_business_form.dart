import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class NewBusinessForm extends StatefulWidget {
  final VoidCallback _newBusinessCreatedCallback;
  final String _userId;

  NewBusinessForm(this._newBusinessCreatedCallback, this._userId);

  @override
  _NewBusinessFormState createState() => _NewBusinessFormState();
}

class _NewBusinessFormState extends State<NewBusinessForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  int _type;
  String _countryId;
  String _name;
  String _pictureUrl = "";
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String docId;
      Business formBusiness = Business(
        active: true,
        userId: widget._userId,
        type: _type,
        countryId: _countryId,
        name: _name,
        pictureUrl: _pictureUrl
      );
      docId = await database.addNewBusiness(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
        widget._newBusinessCreatedCallback();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showTypeInput(),
            showCountryInput(),
            showNameInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showTypeInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _countryId,
        decoration: InputDecoration(
          labelText: 'Tipo de Negocio'
        ),
        items: [
          DropdownMenuItem(child: Text('Casa Hogar / Fundación / Refugio'), value: 0),
          DropdownMenuItem(child: Text('Veterinaria'), value: 1),
          DropdownMenuItem(child: Text('Tienda'), value: 2),
          DropdownMenuItem(child: Text('Paseos / Hospedajes'), value: 3)
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (value) => _type = value,
        onSaved: (value) => _type = value
      )
    );
  }

  Widget showCountryInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _countryId,
        decoration: InputDecoration(
          labelText: 'País'
        ),
        items: [
          DropdownMenuItem(child: Text('Nicaragua'), value: "NIC"),
          DropdownMenuItem(child: Text('El Salvador'), value: "SLV")
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (value) => _countryId = value,
        onSaved: (value) => _countryId = value
      )
    );
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _name,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Nombre'
        ),
        validator: (value) => value.isEmpty ? 'Nombre no puede estar vacío' : null,
        onSaved: (value) => _name = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
