import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class DonationAccountKeyForm extends StatefulWidget {
  final Business _business;
  final String _mapKey;

  DonationAccountKeyForm(this._business, this._mapKey);

  @override
  _DonationAccountKeyFormState createState() => _DonationAccountKeyFormState();
}

class _DonationAccountKeyFormState extends State<DonationAccountKeyForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  Map _businessDonationAccounts = {};
  String _key;
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildBusinessDonationAccounts();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        openingHours: widget._business.openingHours,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        phone: widget._business.phone,
        services: widget._business.services,
        // edit
        donationAccounts: _businessDonationAccounts.cast<String, List<Map<String, String>>>()
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildBusinessDonationAccounts() {
    Map donationAccounts = widget._business.donationAccounts.cast();

    if(widget._mapKey != null) {
      donationAccounts.forEach((key, value) {
        String newKey;
        if(key.toString() == widget._mapKey) {
          newKey = _key;
        } else {
          newKey = key.toString();
        }
        _businessDonationAccounts.putIfAbsent(newKey, () => value);
      });
    } else {
      if(donationAccounts.length > 0) {
        donationAccounts.forEach((key, value) {
          _businessDonationAccounts.putIfAbsent(key, () => value);
        });
        _businessDonationAccounts.putIfAbsent(_key, () => []);
      } else {
        _businessDonationAccounts.putIfAbsent(_key, () => []);
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    if(widget._mapKey != null) {
      _key = widget._mapKey;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showKeyInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showKeyInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _key,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre del Banco / Servicio',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Nombre del Banco / Servicio'
        ),
        validator: (value) => value.isEmpty ? 'Nombre del Banco / Servicio no puede estar vacío' : null,
        onSaved: (value) => _key = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
