import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path/path.dart' as Path;

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class GeneralInfoForm extends StatefulWidget {
  final Business _business;

  GeneralInfoForm(this._business);

  @override
  _GeneralInfoFormState createState() => _GeneralInfoFormState();
}

class _GeneralInfoFormState extends State<GeneralInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  String _countryId;
  String _name;
  String _pictureUrl = "";
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        phone: widget._business.phone,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        services: widget._business.services,
        donationAccounts: widget._business.donationAccounts,
        openingHours: widget._business.openingHours,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        // edit
        countryId: _countryId,
        name: _name,
        pictureUrl: _pictureUrl != null || _pictureUrl != "" ? _pictureUrl : ""
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    if(widget._business != null) {
      _countryId = widget._business.countryId;
      _name = widget._business.name;
      _pictureUrl = widget._business.pictureUrl;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showPictureInput(),
            showCountryInput(),
            showNameInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showPictureInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _pictureUrl == null ?
             Image(height: 250, image: AssetImage(PlaceholderImages.placeholderBanner)) :
             FadeInImage(
               height: 250,
               image: NetworkImage(_pictureUrl),
               placeholder: AssetImage(PlaceholderImages.placeholderBanner),
               fit: BoxFit.contain
             ),
            Padding(padding: EdgeInsets.all(5)),
            RaisedButton(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)
              ),
              color: CustomColors.mainGreen,
              child: Text('SUBIR IMAGEN', style: TextStyle(fontSize: 20.0, color: Colors.white)),
              onPressed: () => pickImage()
            )
          ]
        )
      )
    );
  }

  Future pickImage() async {
    var imageFullInfo = await ImagePickerWeb.getImageInfo;
    if (imageFullInfo.data != null) {
      String mimeType = mime(Path.basename(imageFullInfo.fileName));
      String fileUrl = await database.uploadBusinessImage(
        widget._business.type,
        widget._business.name,
        imageFullInfo.fileName.replaceAll(' ', '_'),
        imageFullInfo.data,
        mimeType
      );
      setState(() {
        _pictureUrl = fileUrl;
      });
    }
  }

  Widget showCountryInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _countryId,
        decoration: InputDecoration(
          labelText: 'País'
        ),
        items: [
          DropdownMenuItem(child: Text('Nicaragua'), value: "NIC"),
          DropdownMenuItem(child: Text('El Salvador'), value: "SLV")
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (value) => _countryId = value,
        onSaved: (value) => _countryId = value
      )
    );
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _name,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Nombre'
        ),
        validator: (value) => value.isEmpty ? 'Nombre no puede estar vacío' : null,
        onSaved: (value) => _name = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
