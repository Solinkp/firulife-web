import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class ScheduleInfoForm extends StatefulWidget {
  final Business _business;

  ScheduleInfoForm(this._business);

  @override
  _ScheduleInfoFormState createState() => _ScheduleInfoFormState();
}

class _ScheduleInfoFormState extends State<ScheduleInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _openingHours;
  String _monday;
  String _tuesday;
  String _wednesday;
  String _thursday;
  String _friday;
  String _saturday;
  String _sunday;
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildOpeningHours();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        phone: widget._business.phone,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        services: widget._business.services,
        donationAccounts: widget._business.donationAccounts,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        // edit
        openingHours: _openingHours
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildOpeningHours() {
    _openingHours = [];
    _openingHours = [
      {
        'day': 'Lunes',
        'schedule': _monday
      },
      {
        'day': 'Martes',
        'schedule': _tuesday
      },
      {
        'day': 'Miércoles',
        'schedule': _wednesday
      },
      {
        'day': 'Jueves',
        'schedule': _thursday
      },
      {
        'day': 'Viernes',
        'schedule': _friday
      },
      {
        'day': 'Sábado',
        'schedule': _saturday
      },
      {
        'day': 'Domingo',
        'schedule': _sunday
      }
    ];
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _openingHours = widget._business.openingHours.cast<Map<String, String>>();
    List localOpeningHours = _openingHours.cast();
    if(localOpeningHours != null && localOpeningHours.isNotEmpty) {
      _monday = localOpeningHours[0]['schedule'];
      _tuesday = localOpeningHours[1]['schedule'];
      _wednesday = localOpeningHours[2]['schedule'];
      _thursday = localOpeningHours[3]['schedule'];
      _friday = localOpeningHours[4]['schedule'];
      _saturday = localOpeningHours[5]['schedule'];
      _sunday = localOpeningHours[6]['schedule'];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showMondayInput(),
            showTuesdayInput(),
            showWednesdayInput(),
            showThursdayInput(),
            showFridayInput(),
            showSaturdayInput(),
            showSundayInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

    Widget showMondayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _monday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Lunes (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Lunes'
        ),
        onSaved: (value) => value.isEmpty ? _monday = 'CERRADO' : _monday = value.trim()
      )
    );
  }

  Widget showTuesdayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _tuesday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Martes (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Martes'
        ),
        onSaved: (value) => value.isEmpty ? _tuesday = 'CERRADO' : _tuesday = value.trim()
      )
    );
  }

  Widget showWednesdayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _wednesday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Miércoles (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Miércoles'
        ),
        onSaved: (value) => value.isEmpty ? _wednesday = 'CERRADO' : _wednesday = value.trim()
      )
    );
  }

  Widget showThursdayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _thursday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Jueves (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Jueves'
        ),
        onSaved: (value) => value.isEmpty ? _thursday = 'CERRADO' : _thursday = value.trim()
      )
    );
  }

  Widget showFridayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _friday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Viernes (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Viernes'
        ),
        onSaved: (value) => value.isEmpty ? _friday = 'CERRADO' : _friday = value.trim()
      )
    );
  }

  Widget showSaturdayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _saturday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Sábado (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Sábado'
        ),
        onSaved: (value) => value.isEmpty ? _saturday = 'CERRADO' : _saturday = value.trim()
      )
    );
  }

  Widget showSundayInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _sunday,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Horario del día Domingo (Dejar vacío si no se abre este día)',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          ),
          helperText: 'Domingo'
        ),
        onSaved: (value) => value.isEmpty ? _sunday = 'CERRADO' : _sunday = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
