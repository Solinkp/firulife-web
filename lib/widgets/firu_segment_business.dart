import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';
import '../settings/new_business_arguments.dart';
import '../models/business.dart';
import '../screens/shared/show_business_info_screen.dart';
import '../screens/shared/show_business_products_screen.dart';
import '../screens/shared/show_business_pets_screen.dart';
import '../screens/shared/show_donation_info_screen.dart';
import '../screens/shared/new_business_form_screen.dart';

class FiruSegmentBusiness extends StatelessWidget {
  final Business _business;
  final String _title;
  final int _index;
  final NewBusinessArguments _businessArguments;

  FiruSegmentBusiness(this._business, this._title, this._index, this._businessArguments);

  void goToSelectedSegment(BuildContext context) {
    switch(_index) {
      case 0:
        Navigator.of(context).pushNamed(ShowBusinessInfoScreen.routeName, arguments: _business);
      break;
      case 1:
        Navigator.of(context).pushNamed(ShowBusinessProductsScreen.routeName, arguments: _business);
      break;
      case 2:
        Navigator.of(context).pushNamed(ShowBusinessPetsScreen.routeName, arguments: _business);
      break;
      case 3:
        Navigator.of(context).pushNamed(ShowDonationInfoScreen.routeName, arguments: _business);
      break;
      case 4:
        Navigator.of(context).pushNamed(NewBusinessFormScreen.routeName, arguments: _businessArguments);
      break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => goToSelectedSegment(context),
      splashColor: Theme.of(context).accentColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        child: Center(child: Text(_title, style: TextStyle(color: Colors.white), textAlign: TextAlign.center)),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [CustomColors.mainGreen.withOpacity(0.7), CustomColors.mainGreen],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight
          ),
          borderRadius: BorderRadius.circular(15)
        ),
      ),
    );
  }
}
