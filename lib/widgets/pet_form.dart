import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path/path.dart' as Path;

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../models/pet.dart';
import './spinner_loader.dart';

class PetForm extends StatefulWidget {
  final Pet _pet;
  final String _businessId;
  final String _businessName;

  PetForm(this._pet, this._businessId, this._businessName);

  @override
  _PetFormState createState() => _PetFormState();
}

class _PetFormState extends State<PetForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  String _name;
  bool _sex;
  String _age;
  String _description;
  String _pictureUrl = "";
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate() && _age != null) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String docId;
      Pet formPet = Pet(
        id: widget._pet != null ? widget._pet.id : null,
        businessId: widget._businessId,
        name: _name,
        sex: _sex,
        age: _age,
        description: _description,
        pictureUrl: _pictureUrl != null || _pictureUrl != "" ? _pictureUrl : ""
      );
      if(widget._pet != null) {
        docId = await database.editPet(formPet);
      } else {
        docId = await database.createPet(formPet);
      }
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    if(widget._pet != null) {
      _name = widget._pet.name;
      _sex = widget._pet.sex;
      _age = widget._pet.age;
      _description = widget._pet.description;
      _pictureUrl = widget._pet.pictureUrl;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
     return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showPictureInput(),
            showNameInput(),
            showSexInput(),
            showAgeInput(context),
            showDescriptionInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showPictureInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _pictureUrl == null ?
             Image(height: 250, image: AssetImage(PlaceholderImages.placeholderFiru)) :
             FadeInImage(
               height: 250,
               image: NetworkImage(_pictureUrl),
               placeholder: AssetImage(PlaceholderImages.placeholderFiru),
               fit: BoxFit.contain
             ),
            Padding(padding: EdgeInsets.all(5)),
            RaisedButton(
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)
              ),
              color: CustomColors.mainGreen,
              child: Text('SUBIR IMAGEN', style: TextStyle(fontSize: 20.0, color: Colors.white)),
              onPressed: () => pickImage()
            )
          ]
        )
      )
    );
  }

  Future pickImage() async {
    var imageFullInfo = await ImagePickerWeb.getImageInfo;
    if (imageFullInfo.data != null) {
      String mimeType = mime(Path.basename(imageFullInfo.fileName));
      String fileUrl = await database.uploadPetImage(
        widget._businessName,
        imageFullInfo.fileName.replaceAll(' ', '_'),
        imageFullInfo.data,
        mimeType
      );
      setState(() {
        _pictureUrl = fileUrl;
      });
    }
  }

  Widget showNameInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _name,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Nombre',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Nombre no puede estar vacío' : null,
        onSaved: (value) => _name = value.trim()
      )
    );
  }

  Widget showSexInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _sex,
        decoration: InputDecoration(
          labelText: 'Género'
        ),
        items: [
          DropdownMenuItem(child: Text('Macho'), value: true),
          DropdownMenuItem(child: Text('Hembra'), value: false)
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (_) {},
        onSaved: (value) => _sex = value
      )
    );
  }

  Widget showAgeInput(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: Row(children: [
        IconButton(
          icon: Icon(Icons.calendar_today),
          color: CustomColors.mainDark,
          onPressed: () => pickDate(context)
        ),
        Text(
          _age == null ? 'Click en el ícono de calendario para definir la fecha de Nacimiento' : _age,
          style: TextStyle(color: _age == null ? Colors.red : Colors.grey),
        )
      ])
    );
  }

  Future<void> pickDate(BuildContext context) async {
    final int firstDate = DateTime.now().year - 12;
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _age == null ? DateTime.now() : DateTime(DateTime.parse(_age).year, DateTime.parse(_age).month, DateTime.parse(_age).day),
      firstDate: DateTime(firstDate),
      lastDate: DateTime.now()
    );
    if(date != null) {
      String finalDate = date.toString().split(" ")[0];
      setState(() {
        _age = finalDate;
      });
    }
  }

  Widget showDescriptionInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _description,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Descripción',
          icon: Icon(
            Icons.description,
            color: CustomColors.mainDark
          )
        ),
        validator: (value) => value.isEmpty ? 'Descripción Completa no puede estar vacío' : null,
        onSaved: (value) => _description = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
