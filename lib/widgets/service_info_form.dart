import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class ServiceInfoForm extends StatefulWidget {
  final Business _business;
  final int _index;

  ServiceInfoForm(this._business, this._index);

  @override
  _ServiceInfoFormState createState() => _ServiceInfoFormState();
}

class _ServiceInfoFormState extends State<ServiceInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  List<String> _businessService = [];
  String _service;
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildBusinessService();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        donationAccounts: widget._business.donationAccounts,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        openingHours: widget._business.openingHours,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        phone: widget._business.phone,
        // edit
        services: _businessService
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildBusinessService() {
    if(widget._index == null) {
      _businessService.add(_service);
    } else {
      _businessService[widget._index] = _service;
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _businessService = widget._business.services.cast();
    if(_businessService == null) {
      _businessService = [];
    }
    if(widget._index != null) {
      _service = _businessService[widget._index];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showServiceInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showServiceInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _service,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Servicio',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Servicio'
        ),
        validator: (value) => value.isEmpty ? 'Servicio no puede estar vacío' : null,
        onSaved: (value) => _service = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
