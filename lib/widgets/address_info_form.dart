import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class AddressInfoForm extends StatefulWidget {
  final Business _business;
  final int _index;

  AddressInfoForm(this._business, this._index);

  @override
  _AddressInfoFormState createState() => _AddressInfoFormState();
}

class _AddressInfoFormState extends State<AddressInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  List<Map<String, String>> _businessAddress = [];
  String _address;
  String _coordinates;
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildBusinessAddress();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        services: widget._business.services,
        donationAccounts: widget._business.donationAccounts,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        openingHours: widget._business.openingHours,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        phone: widget._business.phone,
        // edit
        addresses: _businessAddress
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildBusinessAddress() {
    Map<String, String> fullAddress = {};
    fullAddress.putIfAbsent('address', () => _address);
    fullAddress.putIfAbsent('coordinates', () => _coordinates);
    if(widget._index == null) {
      _businessAddress.add(fullAddress);
    } else {
      _businessAddress[widget._index] = fullAddress;
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _businessAddress = widget._business.addresses;
    if(_businessAddress == null) {
      _businessAddress = [];
    }
    if(widget._index != null) {
      List addresses = _businessAddress.cast();
      _address = addresses[widget._index]['address'];
      _coordinates = addresses[widget._index]['coordinates'];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showAddressInput(),
            showCoordinatesInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showAddressInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _address,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Dirección',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Dirección'
        ),
        validator: (value) => value.isEmpty ? 'Dirección no puede estar vacía' : null,
        onSaved: (value) => _address = value.trim()
      )
    );
  }

  Widget showCoordinatesInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _coordinates == null ? "" : _coordinates,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Coordenadas exactas o referencia de Google Maps (opcional)',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Coordenadas exactas o referencia de Google Maps (opcional)'
        ),
        onSaved: (value) => _coordinates = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
