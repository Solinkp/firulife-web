import 'package:flutter/material.dart';

import '../models/business.dart';
import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../settings/address_arguments.dart';
import '../settings/phone_arguments.dart';
import '../settings/service_arguments.dart';
import '../screens/shared/general_info_form_screen.dart';
import '../screens/shared/schedule_info_form_screen.dart';
import '../screens/shared/contact_info_form_screen.dart';
import '../screens/shared/address_info_form_screen.dart';
import '../screens/shared/phone_info_form_screen.dart';
import '../screens/shared/service_info_form_screen.dart';

class InfoPanel extends StatefulWidget {
  final String title;
  final int type;
  final Business business;

  InfoPanel({@required this.title, @required this.type, this.business});

  @override
  _InfoPanelState createState() => _InfoPanelState();
}

class _InfoPanelState extends State<InfoPanel> {
  final String placeholderImage = PlaceholderImages.placeholderBanner;
  final BaseDatabase database = Database();

  List<Widget> _getChildren(BuildContext context) {
    List<Widget> children = [];
    List<Widget> childrenValue = [];

    switch(widget.type) {
      case 0:
        String countryName = widget.business.countryId == "NIC" ? "Nicaragua" : "El Salvador";
        children.add(
          Column(children: [
            Text('$countryName\n\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
            Text('${widget.business.name}\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
            Container(
              height: 250,
              width: 500,
              color: widget.business.businessColor != null ? Color(int.parse(widget.business.businessColor)) : CustomColors.mainGreen,
              child: Hero(
                tag: 'business_${widget.business.id}',
                child: FadeInImage(
                  image: widget.business.pictureUrl == null ? Image.asset(placeholderImage) : NetworkImage(widget.business.pictureUrl),
                  placeholder: AssetImage(placeholderImage),
                  fit: BoxFit.contain
                )
              )
            ),
            Padding(padding: EdgeInsets.all(10))
          ])
        );
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.edit),
              color: Colors.white,
              tooltip: 'Editar Información Básica',
              onPressed: () => _openSegmentEditForm(context, 0, widget.business)
            )
          )
        );
      break;
      case 1:
        if(widget.business.openingHours != null) {
          List listHours;
          listHours = widget.business.openingHours.cast();
          for(int i = 0; i < listHours.length; i++) {
            children.add(
              Row(children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Text(
                    '${listHours[i]['day']}:',
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: CustomColors.mainDark, fontSize: 16)
                  )
                ),
                Expanded(
                  flex: 3,
                  child: ListTile(
                    title: Text(
                      '${listHours[i]['schedule']}',
                      style: const TextStyle(color: CustomColors.mainDark)
                    )
                  )
                )
              ])
            );
          }
        }
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.edit),
              color: Colors.white,
              tooltip: 'Editar Horarios de Atención',
              onPressed: () => _openSegmentEditForm(context, 1, widget.business)
            ),
          )
        );
      break;
      case 2:
        if(widget.business.contact != null) {
          List listContact;
          listContact = widget.business.contact.cast();
          for(int i = 0; i < listContact.length; i++) {
            List mapKeys = listContact[i].keys.toList();
            List mapValues = listContact[i].values.toList();
            for(int o = 0; o < listContact[i].length; o ++) {
              childrenValue.add(
                Text('${mapKeys[o]}: ${mapValues[o]}\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark))
              );
            }
          }
          children.add(
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: childrenValue
            )
          );
          if(widget.business.type == 0 && widget.business.adoptionLink != null && widget.business.adoptionLink != "") {
            children.add(Text('Información de adopción: ${widget.business.adoptionLink}\n', style: const TextStyle(fontSize: 16, color: CustomColors.mainDark, fontWeight: FontWeight.bold), textAlign: TextAlign.center));
          }
        }
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.edit),
              color: Colors.white,
              tooltip: 'Editar Redes de Contacto',
              onPressed: () => _openSegmentEditForm(context, 2, widget.business)
            ),
          )
        );
      break;
      case 3:
        if(widget.business.addresses != null) {
          List addresses = widget.business.addresses.cast();
          if(addresses != null && addresses.length > 0) {
            for(int i = 0; i < addresses.length; i ++) {
              children.add(
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: ListTile(
                          leading: Icon(Icons.location_on, color: CustomColors.mainGreen),
                          title: Text(addresses[i]['address'])
                        )
                      ),
                      Expanded(
                        flex: 2,
                        child: Row(children: [
                          IconButton(
                            icon: Icon(Icons.edit),
                            color: CustomColors.mainDark,
                            tooltip: 'Editar Dirección',
                            onPressed: () => _openItemEditForm(context, 3, widget.business, i)
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            color: CustomColors.mainDark,
                            tooltip: 'Eliminar Dirección',
                            onPressed: () => _deleteItem(context, addresses, i)
                          )
                        ])
                      )
                    ]
                  )
                )
              );
              children.add(Divider());
            }
          }
        }
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.add),
              color: Colors.white,
              tooltip: 'Agregar Nueva Dirección',
              onPressed: () => _openItemAddForm(3, widget.business)
            )
          )
        );
      break;
      case 4:
        if(widget.business.phone != null) {
          List phones = widget.business.phone.cast();
          if(phones != null && phones.length > 0) {
            for(int i = 0; i < phones.length; i ++) {
              children.add(
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: ListTile(
                          leading: Icon(Icons.phone, color: CustomColors.mainGreen),
                          title: Text(phones[i])
                        )
                      ),
                      Expanded(
                        flex: 2,
                        child: Row(children: [
                          IconButton(
                            icon: Icon(Icons.edit),
                            color: CustomColors.mainDark,
                            tooltip: 'Editar Teléfono',
                            onPressed: () => _openItemEditForm(context, 4, widget.business, i)
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            color: CustomColors.mainDark,
                            tooltip: 'Eliminar Teléfono',
                            onPressed: () => _deleteItem(context, phones, i)
                          )
                        ])
                      )
                    ]
                  )
                )
              );
            }
          }
        }
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.add),
              color: Colors.white,
              tooltip: 'Agregar Nuevo Teléfono',
              onPressed: () => _openItemAddForm(4, widget.business)
            )
          )
        );
      break;
      case 5:
        if(widget.business.services != null) {
          List services = widget.business.services.cast();
          if(services != null && services.length > 0) {
            for(int i = 0; i < services.length; i ++) {
              children.add(
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: ListTile(
                          title: Text('- ${services[i]}', style: const TextStyle(color: CustomColors.mainDark))
                        )
                      ),
                      Expanded(
                        flex: 2,
                        child: Row(children: [
                          IconButton(
                            icon: Icon(Icons.edit),
                            color: CustomColors.mainDark,
                            tooltip: 'Editar Servicio',
                            onPressed: () => _openItemEditForm(context, 5, widget.business, i)
                          ),
                          IconButton(
                            icon: Icon(Icons.delete),
                            color: CustomColors.mainDark,
                            tooltip: 'Eliminar Servicio',
                            onPressed: () => _deleteItem(context, services, i)
                          )
                        ])
                      )
                    ]
                  )
                )
              );
            }
          }
        }
        children.add(
          Container(
            color: CustomColors.mainGreen,
            child: IconButton(
              icon: Icon(Icons.add),
              color: Colors.white,
              tooltip: 'Agregar Nuevo Servicio',
              onPressed: () => _openItemAddForm(5, widget.business)
            )
          )
        );
      break;
    }
    return children;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: ExpansionTile(
          title: Container(color: CustomColors.mainGreen, child: Center(child: Text(widget.title, style: const TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold)))),
          children: _getChildren(context)
        )
      )
    );
  }

  void _openSegmentEditForm(BuildContext context, int segment, Business business) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡AVISO!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                switch(segment) {
                  case 0:
                    Navigator.of(context).pushNamed(GeneralInfoFormScreen.routeName, arguments: business);
                  break;
                  case 1:
                    Navigator.of(context).pushNamed(ScheduleInfoFormScreen.routeName, arguments: business);
                  break;
                  case 2:
                    Navigator.of(context).pushNamed(ContactInfoFormScreen.routeName, arguments: business);
                  break;
                }
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _openItemAddForm(int segment, Business business) {
    switch(segment) {
      case 3:
        Navigator.of(context).pushNamed(AddressInfoFormScreen.routeName, arguments: AddressArguments(business, null));
      break;
      case 4:
        Navigator.of(context).pushNamed(PhoneInfoFormScreen.routeName, arguments: PhoneArguments(business, null));
      break;
      case 5:
        Navigator.of(context).pushNamed(ServiceInfoFormScreen.routeName, arguments: ServiceArguments(business, null));
      break;
    }
}

  void _openItemEditForm(BuildContext context, int segment, Business business, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡AVISO!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea modificar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
                switch(segment) {
                  case 3:
                    Navigator.of(context).pushNamed(AddressInfoFormScreen.routeName, arguments: AddressArguments(business, index));
                  break;
                  case 4:
                    Navigator.of(context).pushNamed(PhoneInfoFormScreen.routeName, arguments: PhoneArguments(business, index));
                  break;
                  case 5:
                    Navigator.of(context).pushNamed(ServiceInfoFormScreen.routeName, arguments: ServiceArguments(business, index));
                  break;
                }
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _deleteItem(BuildContext context, List list, int index) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: CustomColors.mainDark),
              children: <TextSpan>[
                TextSpan(text: '¡ADVERTENCIA!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro que desea eliminar el registro?')
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text('SI', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () async {
                setState(() {
                  list.removeAt(index);
                });
                String docId = await database.editBusinessInfo(widget.business);
                if(docId != null) {
                  Navigator.of(context).pop();
                }
              }
            ),
            FlatButton(
              child: Text('NO', style: const TextStyle(color: CustomColors.mainGreen)),
              onPressed: () {
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}
