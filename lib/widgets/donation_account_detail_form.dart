import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class DonationAccountDetailForm extends StatefulWidget {
  final Business _business;
  final String _mapKey;
  final int _index;

  DonationAccountDetailForm(this._business, this._mapKey, this._index);

  @override
  _DonationAccountDetailFormState createState() => _DonationAccountDetailFormState();
}

class _DonationAccountDetailFormState extends State<DonationAccountDetailForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  String _key; // Cordobas / Dolares etc
  String _detail; // numero de cuenta / correo etc
  String _errorMessage;
  bool _isLoading;
  //
  Map _donationAccounts;
  List _accounts;
  Map _accountDetails;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      updateBusinessDonationAccount();
      String docId;
      docId = await database.editBusinessInfo(widget._business);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void updateBusinessDonationAccount() {
    Map newMap = {_key: _detail};
    if(widget._index == null) {
      _accounts.add(newMap);
    } else {
      _accounts[widget._index] = newMap;
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _donationAccounts = widget._business.donationAccounts.cast();
    _accounts = _donationAccounts[widget._mapKey];
    if(widget._index != null) {
      _accountDetails = _accounts[widget._index];
      _key = _accountDetails.keys.single.toString();
      _detail = _accountDetails.values.single.toString();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showKeyInput(),
            showDetailInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showKeyInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
      child: DropdownButtonFormField(
        value: _key,
        decoration: InputDecoration(
          labelText: 'Moneda / Tipo de Servicio'
        ),
        items: [
          DropdownMenuItem(child: Text('Córdobas C\$'), value: 'Córdobas C\$'),
          DropdownMenuItem(child: Text('Dólares \$'), value: 'Dólares \$'),
          DropdownMenuItem(child: Text('Correo'), value: 'Correo'),
          DropdownMenuItem(child: Text('Banca Móvil'), value: 'Banca Móvil'),
          DropdownMenuItem(child: Text('Enlace'), value: 'Enlace')
        ],
        validator: (value) {
          if(value == null) {
            return 'Campo requerido';
          }
          return null;
        },
        onChanged: (value) => _key = value,
        onSaved: (value) => _key = value
      )
    );
  }

  Widget showDetailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _detail,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Número de cuenta / Correo / etc...',
          icon: Icon(
            Icons.text_fields,
            color: CustomColors.mainDark
          ),
          helperText: 'Número de cuenta / Correo / etc...'
        ),
        validator: (value) => value.isEmpty ? 'Este campo no puede estar vacío' : null,
        onSaved: (value) => _detail = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
