import 'package:flutter/material.dart';

import '../services/database.dart';
import '../settings/custom_colors.dart';
import '../models/business.dart';
import './spinner_loader.dart';

class PhoneInfoForm extends StatefulWidget {
  final Business _business;
  final int _index;

  PhoneInfoForm(this._business, this._index);

  @override
  _PhoneInfoFormState createState() => _PhoneInfoFormState();
}

class _PhoneInfoFormState extends State<PhoneInfoForm> {
  final BaseDatabase database = Database();
  final _formKey = GlobalKey<FormState>();
  List<String> _businessPhone = [];
  String _phone;
  String _errorMessage;
  bool _isLoading;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit(BuildContext context) async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      buildBusinessPhone();
      String docId;
      Business formBusiness = Business(
        // preset
        id: widget._business.id,
        addresses: widget._business.addresses,
        type: widget._business.type,
        businessColor: widget._business.businessColor,
        services: widget._business.services,
        donationAccounts: widget._business.donationAccounts,
        countryId: widget._business.countryId,
        name: widget._business.name,
        pictureUrl: widget._business.pictureUrl,
        openingHours: widget._business.openingHours,
        contact: widget._business.contact,
        adoptionLink: widget._business.adoptionLink,
        // edit
        phone: _businessPhone
      );
      docId = await database.editBusinessInfo(formBusiness);
      if(docId != null) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pop();
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void buildBusinessPhone() {
    if(widget._index == null) {
      _businessPhone.add(_phone);
    } else {
      _businessPhone[widget._index] = _phone;
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _businessPhone = widget._business.phone.cast();
    if(_businessPhone == null) {
      _businessPhone = [];
    }
    if(widget._index != null) {
      _phone = _businessPhone[widget._index];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _showForm(context),
        _showLoader()
      ]
    );
  }

  Widget _showForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            showPhoneInput(),
            showPrimaryButton(context),
            showSecondaryButton(context),
            showErrorMessage()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader());
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget showPhoneInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
      child: TextFormField(
        initialValue: _phone,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'Teléfono',
          icon: Icon(
            Icons.phone,
            color: CustomColors.mainDark
          ),
          helperText: 'Teléfono'
        ),
        // regex for phone number not letters
        validator: (value) => value.isEmpty ? 'Teléfono no puede estar vacío' : null,
        onSaved: (value) => _phone = value.trim()
      )
    );
  }

  Widget showPrimaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainGreen,
          child: Text('GUARDAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => validateAndSubmit(context)
        )
      )
    );
  }

  Widget showSecondaryButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: RaisedButton(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)
          ),
          color: CustomColors.mainDark,
          child: Text('CANCELAR', style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: () => Navigator.of(context).pop()
        )
      )
    );
  }

  Widget showErrorMessage() {
    if(_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: Colors.red,
          height: 1.0,
          fontWeight: FontWeight.w300
        )
      );
    } else {
      return Container(height: 0.0);
    }
  }

}
