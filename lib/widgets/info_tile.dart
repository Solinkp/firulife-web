import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class InfoTile extends StatelessWidget {
  final IconData icon;
  final String title;

  InfoTile(this.icon, this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        leading: Icon(icon, color: CustomColors.mainGreen),
        title: Text(title)
      )
    );
  }
}
